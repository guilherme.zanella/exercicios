package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class ExcluirPessoa {
    public static void main(String[] args) throws SQLException {
        Scanner entrada = new Scanner(System.in);
        Connection conexao = FabricaConexao.getConexao();

        System.out.print("Informe o codigo da pessoa: ");
        String sql = "delete from pessoas where codigo = ?";

        int id = entrada.nextInt();

        PreparedStatement stmt = conexao.prepareStatement(sql);
        stmt.setInt(1, id);

        if(stmt.executeUpdate() > 0) {
            System.out.println("Pessoa excluida com sucesso!");
        } else {
            System.out.println("Nada aconteceu.");
        }

        conexao.close();
        entrada.close();
    }
}
