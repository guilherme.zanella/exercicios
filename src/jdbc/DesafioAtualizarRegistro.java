package jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class DesafioAtualizarRegistro {
    public static void main(String[] args) throws SQLException {
        Scanner entrada = new Scanner(System.in);
        Connection conexao = FabricaConexao.getConexao();

        System.out.print("Informe o codigo da pessoa: ");
        int pesquisa = entrada.nextInt();

        String select = "select * from pessoas where codigo = ?";
        String update = "update pessoas set nome = ? where codigo = ?";

        PreparedStatement stmt = conexao.prepareStatement(select);
        stmt.setInt(1, pesquisa);

        ResultSet r = stmt.executeQuery();

        if(r.next()) {
            Pessoa p = new Pessoa(r.getInt(1), r.getString(2));

            System.out.println("Nome atual da pessoa: " + p.getNome());
            entrada.nextLine();
            System.out.print("Informe o novo nome: ");
            String novoNome = entrada.nextLine();

            stmt.close();
            stmt = conexao.prepareStatement(update);
            stmt.setString(1, novoNome);
            stmt.setInt(2, pesquisa);
            stmt.execute();
            stmt.close();

            System.out.println("Nome alterado com sucesso!");
        } else {
            System.out.println("Pessoa nao encontrada.");
            stmt.close();
        }

        entrada.close();
        conexao.close();
    }
}
