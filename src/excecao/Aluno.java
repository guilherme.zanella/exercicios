package excecao;

import java.util.Objects;

public class Aluno {
    public final String nome;
    public final double nota;
    public final boolean bomComportamento;

    public Aluno(String nome, double nota) {
        this(nome, nota, true);
    }

    public Aluno(String nome, double nota, boolean bomComportamento) {
        super();
        this.nome = nome;
        this.nota = nota;
        this.bomComportamento = bomComportamento;
    }

    public String toString() {
        return nome + " tem nota " + nota;
    }

    public int hashCode() {
        return Objects.hash(bomComportamento, nome, nota);
    }

    public boolean equals(Objects obj) {
        if (this.equals(obj)) return true;
        if (obj.equals(null)) return false;
        if (!getClass().equals(obj.getClass())) return false;
//        Aluno other = (Aluno) obj;
//        return bomComportamento == other.bomComportamento && Objects.equals(nome, other.nome)
//                && Double.doubleToLongBits(nota) == Double.doubleToLongBits(other.nota);
        return true;
    }
}
