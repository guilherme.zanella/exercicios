package excecao;

public class ChecadaVsNaoChecada {
    public static void main(String[] args) {
        try {
            geraErro1();
        } catch (RuntimeException e) {
            System.out.println(e.getMessage());
        }

        try {
            geraErro2();
        } catch (Throwable e) {
            System.out.println(e.getMessage());
        }

        System.out.println("Fim.");
    }

    // Exceção NÃO checada/NÃO verificada
    static void geraErro1() {
        throw new RuntimeException("Ocorreu um erro 1.");
    }

    // Exceção checada/verificada
    static void geraErro2() throws Exception {
        throw new Exception("Ocorreu um erro 2.");
    }

	/*
	Exceptions precisam ser tratadas obrigatóriamente para que o programa
	possa ser executado, RuntimeExceptions não.
	*/
}
