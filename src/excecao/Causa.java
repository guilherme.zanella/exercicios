package excecao;

public class Causa {
    public static void main(String[] args) {
        try {
            metodoA(null);
        } catch (Exception e) {
            if(e.getCause() != null) {
                System.out.println(e.getCause().getMessage());
            }
        }
    }

    static void metodoA(Aluno a) {
        try {
            metodoB(a);
        } catch (Exception e) {
            throw new IllegalArgumentException(e);
        }
    }
    static void metodoB(Aluno a) {
        if(a == null) {
            throw new NullPointerException("Aluno esta NULO!!!!");
        }

        System.out.println(a.nome);
    }
}
