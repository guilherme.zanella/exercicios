package observer;

public class Namorada implements ObsChegadaAniversariante {
    @Override
    public void chegou(ChegadaAniversariante evento) {
        System.out.println("Avisar os convidados...");
        System.out.println("Apagar as luzes...");
        System.out.println("Esperar um pouco...");
        System.out.println("e... surpresa!!!");
    }
}
