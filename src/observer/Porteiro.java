package observer;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Porteiro {
    private List<ObsChegadaAniversariante> obs = new ArrayList<>();

    public void registrarObservador(ObsChegadaAniversariante o) {
        obs.add(o);
    }

    public void monitorar() {
        Scanner entrada = new Scanner(System.in);

        String valor = "";

        while(!valor.equalsIgnoreCase("sim")) {
            System.out.println("Aniversariante chegou? ");
            valor = entrada.nextLine();

            if(valor.equalsIgnoreCase("sim")) {
                ChegadaAniversariante evento = new ChegadaAniversariante(new Date());
                obs.stream().forEach(o -> o.chegou(evento));
            } else {
                System.out.println("Alarme falso!");
            }
        }
        entrada.close();
    }
}
