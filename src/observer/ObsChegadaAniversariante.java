package observer;

public interface ObsChegadaAniversariante {
    public void chegou(ChegadaAniversariante evento);
}
