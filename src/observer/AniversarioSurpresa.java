package observer;

public class AniversarioSurpresa {
    public static void main(String[] args) {
        Namorada n = new Namorada();
        Porteiro p = new Porteiro();

        p.registrarObservador(n);
        p.registrarObservador(e -> {
            System.out.println("Surpresa via lambda!");
            System.out.println("Ocorreu em: " + e.getMomento());
        });
        p.monitorar();
    }
}
