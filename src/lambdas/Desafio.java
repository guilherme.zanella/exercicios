package lambdas;

import java.util.function.Function;
import java.util.function.UnaryOperator;

public class Desafio {
    public static void main(String[] args) {
        Produto p = new Produto("iPad", 3235.89, 0.13);

        Function<Produto, Double> precoReal = p1 -> p1.preco * (1 - p1.desconto);
        UnaryOperator<Double> impostoMunicipal = preco -> preco >= 2500 ? preco * 1.085 : preco;
        UnaryOperator<Double> frete = preco -> preco >= 3000 ? preco + 100 : preco + 50;
        UnaryOperator<Double> arredondar = preco -> Math.round(preco * 100.0) / 100.0;
        Function<Double, String> formatar = preco -> "R$" + preco.toString().replace(".", ",");

        System.out.println("Total: " +
                precoReal
                        .andThen(impostoMunicipal)
                        .andThen(frete)
                        .andThen(arredondar)
                        .andThen(formatar)
                        .apply(p));
    }
}
