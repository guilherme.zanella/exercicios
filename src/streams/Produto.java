package streams;

public class Produto {
    final private String nome;
    private double preco;
    private double desconto;
    private boolean frete;

    public Produto(String nome, double preco, double desconto, boolean frete) {
        this.nome = nome;
        this.preco = preco;
        this.desconto = desconto;
        this.frete = frete;
    }

    public double getPreco() {
        return preco;
    }
    public void setPreco(double preco) {
        this.preco = preco;
    }

    public double getDesconto() {
        return desconto;
    }
    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public boolean isFreteGratis() {
        return frete;
    }
    public void setFrete(boolean frete) {
        this.frete = frete;
    }

    public String getNome() {
        return nome;
    }
}
