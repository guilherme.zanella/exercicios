package streams;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

public class ImprimindoObjetos {
    public static void main(String[] args) {
        List<String> aprovados = Arrays.asList("Pedro", "Lucas", "Marcio", "Ana");

        System.out.println("For:");
        for(int i = 0; i < aprovados.size(); i++) {
            System.out.println(aprovados.get(i));
        }

        System.out.println("\nForeach:");
        for(String nome: aprovados) {
            System.out.println(nome);
        }

        System.out.println("\nIterator:");
        Iterator<String> it = aprovados.iterator();
        while(it.hasNext()) {
            System.out.println(it.next());
        }

        System.out.println("\nStream:");
        Stream<String> st = aprovados.stream();
        st.forEach(System.out::println); // laço interno
    }
}
