package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class DesafioFilter2 {
    public static void main(String[] args) {
        Produto p1 = new Produto("Notebook", 3499.99, 0.35, true);
        Produto p2 = new Produto("TV 50\"", 2700.50, 0.15, false);
        Produto p3 = new Produto("Tablet", 1500.00, 0.40, false);
        Produto p4 = new Produto("Celular", 1800.75, 0.40, true);
        Produto p5 = new Produto("Xbox Series S", 2000.00, 0.20, true);
        Produto p6 = new Produto("Playstation 5", 5000.0, 0.30, true);

        List<Produto> produtos = Arrays.asList(p1, p2, p3, p4, p5, p6);

        Predicate<Produto> desconto = p -> p.getDesconto() >= 0.30;
        Predicate<Produto> freteGratis = p -> p.isFreteGratis();
        Function<Produto, String> promocao = p -> {
            return p.getNome() + " com " + (p.getDesconto() * 100) + "% de desconto. \nDe R$" + p.getPreco() + "\nPor R$" + (p.getPreco() * (1 - p.getDesconto())) + "\n";
        };

        produtos.stream()
                .filter(desconto)
                .filter(freteGratis)
                .map(promocao)
                .forEach(System.out::println);
    }
}
