package streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class DesafioFilter1 {
    public static void main(String[] args) {
        // 2 filtros (2 funcoes lambda)
        // 1 map
        Condutor c1 = new Condutor("Ana", 20, true);
        Condutor c2 = new Condutor("Bia", 27, false);
        Condutor c3 = new Condutor("Daniel", 30, true);
        Condutor c4 = new Condutor("Gui", 19, true);
        Condutor c5 = new Condutor("Julia", 22, true);
        Condutor c6 = new Condutor("Pedro", 40, true);

        List<Condutor> condutores = Arrays.asList(c1, c2, c3, c4, c5, c6);

        Predicate<Condutor> idade = c -> c.idade >= 21;
        Predicate<Condutor> temCnhC = c -> c.cnhC;
        Function<Condutor, String> decisao = c -> {
            return "O(A) condutor(a) " + c.nome + " pode adicionar a categoria D a sua CNH.";
        };

        condutores.stream()
                .filter(idade)
                .filter(temCnhC)
                .map(decisao)
                .forEach(System.out::println);
    }

}
